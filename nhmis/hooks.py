# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from . import __version__ as app_version

app_name = "nhmis"
app_title = "Nhmis"
app_publisher = "omar"
app_description = "National Health Management Information System"
app_icon = "fa fa-medkit"
app_color = "#ce52c0"
app_email = "omar.ja93@gmail.com"
app_license = "MIT"

fixtures = ["Custom Field"]

# fixtures = [{"dt": "Custom Field", "filters": [["name", "in", [
# 		"Patient-address",
# 		"Patient-card_no",
# 		"Patient-column_break_29",
# 		"Patient-contact_address",
# 		"Patient-female",
# 		"Patient-first_contact_with_facility",
# 		"Patient-gsm_number",
# 		"Patient-information_on_next_of_kin",
# 		"Patient-male",
# 		"Patient-name1",
# 		"Patient-referred_in",
# 		"Patient-relationship_with_client",
# 		"Patient-state_of_origin",
# 		"Patient-telephone_no"
# 	]]]},"Patient"]

 
# fixtures = [
# 	{"doctype":"Custom Field", "filters": {"fieldname": "address"}}
# ]



# Includes in <head>
# ------------------

# include js, css files in header of desk.html
# app_include_css = "/assets/nhmis/css/nhmis.css"
# app_include_js = "/assets/nhmis/js/nhmis.js"

# include js, css files in header of web template
# web_include_css = "/assets/nhmis/css/nhmis.css"
# web_include_js = "/assets/nhmis/js/nhmis.js"

# include js in page
# page_js = {"page" : "public/js/file.js"}

# include js in doctype views
# doctype_js = {"doctype" : "public/js/doctype.js"}
# doctype_list_js = {"doctype" : "public/js/doctype_list.js"}
# doctype_tree_js = {"doctype" : "public/js/doctype_tree.js"}
# doctype_calendar_js = {"doctype" : "public/js/doctype_calendar.js"}

# Home Pages
# ----------

# application home page (will override Website Settings)
# home_page = "login"

# website user home page (by Role)
# role_home_page = {
#	"Role": "home_page"
# }

# Website user home page (by function)
# get_website_user_home_page = "nhmis.utils.get_home_page"

# Generators
# ----------

# automatically create page for each record of this doctype
# website_generators = ["Web Page"]

# Installation
# ------------

# before_install = "nhmis.install.before_install"
# after_install = "nhmis.install.after_install"
after_install = "nhmis.nhmis.doctype.referral.referral.add_referal_button"

# Desk Notifications
# ------------------
# See frappe.core.notifications.get_notification_config

# notification_config = "nhmis.notifications.get_notification_config"

# Permissions
# -----------
# Permissions evaluated in scripted ways

# permission_query_conditions = {
# 	"Event": "frappe.desk.doctype.event.event.get_permission_query_conditions",
# }
#
# has_permission = {
# 	"Event": "frappe.desk.doctype.event.event.has_permission",
# }

# Document Events
# ---------------
# Hook on document methods and events

# doc_events = {
# 	"*": {
# 		"on_update": "method",
# 		"on_cancel": "method",
# 		"on_trash": "method"
#	}
# }

# Scheduled Tasks
# ---------------

scheduler_events = {
# 	"all": [
# 		"nhmis.tasks.all"
# 	],
	"daily": [
		"nhmis.nhmis.doctype.antenatal.antenatal.send_checkup_reminders",
        "nhmis.nhmis.doctype.immunization.immunization.send_weekly_notification"
	],
	# "cron": {
 #        '0 0/1 * 1/1 * ? *': [
	# 	"nhmis.nhmis.doctype.immunization.immunization.send_weekly_notification"
	# ]},

# 	"weekly": [
# 		"nhmis.tasks.weekly"
# 	]
# 	"monthly": [
# 		"nhmis.tasks.monthly"
# 	]
}

# Testing
# -------

# before_tests = "nhmis.install.before_tests"

# Overriding Whitelisted Methods
# ------------------------------
#
# override_whitelisted_methods = {
# 	"frappe.desk.doctype.event.event.get_events": "nhmis.event.get_events"
# }

