# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from frappe import _

def get_data():
	return [
		{
			"module_name": "Nhmis",
			"color": "#ce52c0",
			"icon": "fa fa-medkit",
			"type": "module",
			"label": _("NHMIS")
		}
	]
