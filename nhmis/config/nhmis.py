from __future__ import unicode_literals
from frappe import _

def get_data():
	return [
		{
			"label": _("Special Forms"),
			"items": [
				{
					"type": "doctype",
					"name": "Antenatal",
					"label": _("Antenatal"),
					"description":_("Health Facility ANC Daily Register"),
				},
				{
					"type": "doctype",
					"name": "Out Patient",
					"label": _("Out Patient"),
					"description":_("Health Facility Daily OPD Register"),
				},
				{
					"type": "doctype",
					"name": "Family Planning",
					"label": _("Family Planning"),
					"description":_("Health Facility FP Daily Register"),
				},
				{
					"type": "doctype",
					"name": "Nutrition and Growth Planning",
					"label": _("Nutrition and Growth Planning"),
					"description":_("Health Facility GMP Daily Register"),
				},
				{
					"type": "doctype",
					"name": "Immunization",
					"label": _("Immunization"),
					"description":_("Health Facility Immunization Register"),
				},
				{
					"type": "doctype",
					"name": "Child Delivery",
					"label": _("Child Delivery"),
					"description":_("Health Facility LDR Daily Register"),
				},
				{
					"type": "doctype",
					"name": "Daily general attendance register",
					"label": _("Daily general attendance register"),
					"description":_("Daily general attendance register"),
				},
				{
					"type": "doctype",
					"name": "NHMIS QUARTERLY FORM FOR HEALTH FACILITIES",
					"label": _("NHMIS Quarterly Form For Health Facilities"),
					"description":_("NHMIS Quarterly Form For Health Facilities"),
				},
			]
		},
		{
			"label": _("Setup"),
			"items": [
				{
					"type": "doctype",
					"name": "State",
					"label": _("State"),
					"description":_("State"),
				},
				{
					"type": "doctype",
					"name": "Local Government Area",
					"label": _("LGA"),
					"description":_("Local Government Area"),
				},
				{
					"type": "doctype",
					"name": "Ward",
					"label": _("Ward"),
					"description":_("Ward"),
				},
				{
					"type": "doctype",
					"name": "Facility",
					"label": _("Facility"),
					"description":_("Facility"),
				},
				{
					"type": "doctype",
					"name": "Sector",
					"label": _("Sector"),
					"description":_("Sector"),
				},
				{
					"type": "doctype",
					"name": "Service",
					"label": _("Service"),
					"description":_("Service"),
				},
				{
					"type": "doctype",
					"name": "Location",
					"label": _("Location"),
					"description":_("Location"),
				},
				{
					"type": "doctype",
					"name": "Functionality",
					"label": _("Functionality"),
					"description":_("Functionality"),
				},
				{
					"type": "doctype",
					"name": "Care Level",
					"label": _("Care Level"),
					"description":_("Care Level"),
				},
				{
					"type": "doctype",
					"name": "Referral",
					"label": _("Referral"),
					"description":_("Referral"),
				},
				{
					"type": "doctype",
					"name": "Custom SMS",
					"label": _("Custom SMS Setup"),
					"description":_("Custom SMS"),
				}
			]
		},
		{
			"label": _("Reports"),
			"items": [
				{
					"type": "report",
					"is_query_report": True,
					"name": "HEALTH FACILITY ANC DAILY REGISTER",
					"label": _("Health Facility ANC Daily Register"),
					"doctype": "Antenatal"
				},
				{
					"type": "report",
					"is_query_report": True,
					"name": "HEALTH FACILITY DAILY OPD REGISTER",
					"label": _("Health Facility Daily OPD Register"),
					"doctype": "Out Patient"
				},
				{
					"type": "report",
					"is_query_report": True,
					"name": "HEALTH FACILITY FP DAILY REGISTER",
					"label": _("Health Facility FP Daily Register"),
					"doctype": "Family Planning"
				},
				{
					"type": "report",
					"is_query_report": True,
					"name": "HEALTH FACILITY GMP DAILY REGISTER",
					"label": _("Health Facility GMP Daily Register"),
					"doctype": "Nutrition and Growth Planning"
				},
				{
					"type": "report",
					"is_query_report": True,
					"name": "HEALTH FACILITY IMMUNIZATION REGISTER",
					"label": _("Health Facility Immunization Register"),
					"doctype": "Immunization"
				},
				{
					"type": "report",
					"is_query_report": True,
					"name": "HEALTH FACILITY LDR DAILY REGISTER",
					"label": _("Health Facility LDR Daily Register"),
					"doctype": "Child Delivery"
				},
				{
					"type": "report",
					"is_query_report": True,
					"name": "HEALTH FACILITY TT REGISTER",
					"label": _("Health Facility TT Register"),
					"doctype": "Immunization"
				},
				{
					"type": "report",
					"is_query_report": True,
					"name": "National PMTCT ARVs Register for PHCs",
					"label": _("National PMTCT ARVs Register for PHCs"),
					"doctype": "Antenatal"
				},
				{
					"type": "report",
					"is_query_report": True,
					"name": "NHMIS HF daily general attendance register",
					"label": _("National HF daily general attendance register"),
					"doctype": "Daily general attendance register"
				},
				# {
				# 	"type": "report",
				# 	"is_query_report": True,
				# 	"name": "NHMIS MONTHLY SUMMARY FORM FOR HEALTH FACILITIES",
				# 	"label": _("NHMIS Monthly Summary Form For Health Facilities"),
				# 	"doctype": "Antenatal"
				# },
				
			]
		}
	]


