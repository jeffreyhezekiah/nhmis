# -*- coding: utf-8 -*-
# Copyright (c) 2017, omar and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class HEALTHFACILITYDAILYOPDREGISTER(Document):
	def get_age(self):
		age_str = ""
		if self.dob:
			born = getdate(self.dob)
			age = dateutil.relativedelta.relativedelta(getdate(), born)
			age_str = str(age.years) + " year(s) " + str(age.months) + " month(s) " + str(age.days) + " day(s)"
		return age_str
