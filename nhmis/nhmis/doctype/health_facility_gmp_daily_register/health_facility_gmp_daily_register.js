// Copyright (c) 2017, omar and contributors
// For license information, please see license.txt

frappe.ui.form.on('Health Facility GMP Daily Register', {
	refresh: function (frm) {
		
	},
	onload: function (frm) {
		if(frm.doc.dob){
			$(frm.fields_dict['age_html'].wrapper).html("AGE : " + get_age(frm.doc.dob));
			alert(get_age_m(frm.doc.dob));
			$(frm.fields_dict['age_in_months'].wrapper).html("AGE In Month : " + get_age_m(frm.doc.dob));

		}
	}
});

frappe.ui.form.on("Health Facility GMP Daily Register", "dob", function(frm) {
	if(frm.doc.dob){
		var today = new Date();
		var birthDate = new Date(frm.doc.dob);
		var age_str = get_age(frm.doc.dob);
		var age_str_m = get_age_m(frm.doc.dob)
		$(frm.fields_dict['age_html'].wrapper).html("AGE : " + age_str);
		$(frm.fields_dict['age_in_months'].wrapper).html("AGE In Month : " + age_str_m);

	}
});


var get_age = function (birth) {
	var ageMS = Date.parse(Date()) - Date.parse(birth);
	var age = new Date();
	age.setTime(ageMS);
	var years = age.getFullYear() - 1970;
	return years + " Year(s) " + age.getMonth() + " Month(s) " + age.getDate() + " Day(s)";
};



var get_age_m = function (birth) {
	var ageMS = Date.parse(Date()) - Date.parse(birth);
	var age = new Date();
	age.setTime(ageMS);
	var years = age.getFullYear() - 1970;
	total = (years*12) + age.getMonth()
	return total;
};