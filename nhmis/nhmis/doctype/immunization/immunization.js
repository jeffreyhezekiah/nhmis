// Copyright (c) 2017, omar and contributors
// For license information, please see license.txt

frappe.ui.form.on('Immunization', {
	refresh: function (frm) {
		
	},
	onload: function (frm) {
		if(frm.doc.dob){
			$(frm.fields_dict['age_html'].wrapper).html("AGE : " + get_age(frm.doc.dob));
		}
	},
	facility: (frm) => {
		 
		frappe.call({ 
		   method:"frappe.client.get_value",
		    args: {
		        doctype:"Facility",
		        filters: {
		            'facility_name': frm.doc.facility
		        },
		        fieldname: ['ward', 'local_government_area', 'state', 'facility_type']
		    }, 
		    callback: function(r) { 		    
		        resp = r.message
		        frm.set_value('lga', resp.local_government_area);
		        frm.set_value('ward', resp.ward);
		        frm.set_value('sector', resp.facility_type);
		        frm.set_value('state', resp.state); 				
 				frm.refresh();
		    }
		});

	}
});

frappe.ui.form.on("Immunization", "dob", function(frm) {
	if(frm.doc.dob){
		var today = new Date();
		var birthDate = new Date(frm.doc.dob);
		var age_str = get_age(frm.doc.dob);
		$(frm.fields_dict['age_html'].wrapper).html("AGE : " + age_str);
	}
});


var get_age = function (birth) {
	var ageMS = Date.parse(Date()) - Date.parse(birth);
	var age = new Date();
	age.setTime(ageMS);
	var years = age.getFullYear() - 1970;
	return years + " Year(s) " + age.getMonth() + " Month(s) " + age.getDate() + " Day(s)";
};
