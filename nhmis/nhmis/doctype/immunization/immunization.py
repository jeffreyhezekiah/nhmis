# -*- coding: utf-8 -*-
# Copyright (c) 2017, omar and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
# from frappe.core.doctype.sms_settings.sms_settings import send_sms

import datetime, os, configobj, json, records


from rq import Queue
from redis import Redis
redis_conn = Redis()
q = Queue(connection=redis_conn)



class Immunization(Document):
	def get_age(self):
		age_str = ""
		if self.dob:
			born = getdate(self.dob)
			age = dateutil.relativedelta.relativedelta(getdate(), born)
			age_str = str(age.years) + " year(s) " + str(age.months) + " month(s) " + str(age.days) + " day(s)"
		return age_str

# IMMTests = {
#     '0-2wk': 12, # twelve days
#     '0-11m': 42, # 42 weeks 
#     '6w-11m': 42, # 42 weeks earlier
#     '10w-11m': 42,
#     '14w-11m': 42,
#     '6m-11m': 42,
#     '9m-11m': 42,
#     '12m-23m': 94,
#     '18m-23m': 94
# }

IMMTests = {
    '0-2wk': 7, # 7 days
    '0-11m': 25, # 42 weeks 
    '6w-11m': 7, # 42 weeks earlier
    '10w-11m': 15, # weeks 
    '14w-11m': 30, # weeks
    '6m-11m': 30, # weeks 
    '9m-11m': 40, # weeks 
    '12m-23m': 56, # weeks 
    '18m-23m': 80, # weeks 
}



def send_weekly_notification():
    
    msg = frappe.db.get_value("Custom SMS", None, "immunization_sms_reminder")

    cfg_data = configobj.ConfigObj('scheduler.cfg')['app']
    cfg_file = os.path.join(os.path.join(cfg_data['sitename'], cfg_data['config_file']))

    json_content = ''

    with open(cfg_file, 'r') as fl:
        json_content = fl.read()
        
    json_obj = json.loads(json_content)
    db_passwd = json_obj['db_password']
    db_name = json_obj['db_name']

    conn_str = 'mysql+mysqldb://%s:%s@localhost:3306/%s' %(db_name, db_passwd, db_name)

    db = records.Database(conn_str)


    _ttl = 300

    _sql = '''
        SELECT (datediff(curdate(), a.dob)) as days, a.facility, 
               a.phone, a.dob, a.client_gsm_number, a.name_of_client,
               a.client_number,  
               a.opv0_weeks,
               a.hepb0_weeks
        FROM tabImmunization as a           
        WHERE datediff(curdate(), a.dob) <= {} 
        AND a.dob is not null
    '''.format(IMMTests['0-2wk'])
    
    _qry = db.query(_sql).export('json')

    if _qry != '[]':        
        
        q.enqueue(
            background_tasks,
            ttl=_ttl, 
            args=('OPV 0', 'Hep.B 0'),
            kwargs={
                'data': _qry,
                'sms_msg': msg,
                'range': '0-2wk'
            })
    

    _sql = '''
        SELECT (week(curdate()) - week (a.dob)) as weeks, 
               a.facility, a.client_gsm_number, a.name_of_client,
               a.phone, a.dob, 
               a.client_number,  
               a.bcg_months          
        FROM tabImmunization as a           
        WHERE week(curdate()) - week (a.dob)={}             
        AND a.dob is not null
    '''.format(IMMTests['0-11m'])

    _qry = db.query(_sql).export('json')

    if _qry != '[]':
        q.enqueue(
            background_tasks,
            ttl=_ttl, 
            args=('BCG',),
            kwargs={
                'data': _qry,
                'sms_msg': msg,
                'range': '0-11m'               
            })


    _sql = '''
        SELECT (week(curdate()) - week (a.dob)) as weeks, a.facility, 
               a.phone, a.dob, a.client_gsm_number, a.name_of_client,
               a.client_number,  
               a.opv1_weeks, 
               a.penta1_weeks, 
               a.pcv1_weeks
        FROM tabImmunization as a           
        WHERE week(curdate()) - week(a.dob)={}              
        AND a.dob is not null
    '''.format(IMMTests['6w-11m'])
    
    _qry = db.query(_sql).export('json')

    if _qry != '[]':
        q.enqueue(
            background_tasks,
            ttl=_ttl, 
            args=('OPV 1', 'PENTA1', 'PCV1'),
            kwargs={
                'data': _qry,
                'sms_msg': msg,  
                'range': '6w-11m'              
            })


    _sql = '''
        SELECT (week(curdate()) - week (a.dob)) as weeks, 
                a.facility, 
                a.phone, a.dob, a.client_gsm_number, a.name_of_client,
                a.client_number,  
                a.opv2_weeks,  
                a.penta2_weeks, 
                a.pcv2_weeks
        FROM tabImmunization as a           
        WHERE week(curdate()) - week (a.dob)={}              
        AND a.dob is not null
    '''.format(IMMTests['10w-11m'])

    _qry = db.query(_sql).export('json')

    if _qry != '[]':
        q.enqueue(
            background_tasks,
            ttl=_ttl, 
            args=('OPV2', 'PENTA2', 'PCV2'),
            kwargs={
                'data': _qry,
                'sms_msg': msg,  
                'range': '10w-11m'              
            })
    


    _sql = '''
        SELECT (week(curdate()) - week (a.dob)) as weeks, a.facility, 
               a.phone, a.dob, a.client_gsm_number, a.name_of_client,
               a.client_number,  
               a.opv3_weeks,  
               a.penta3_weeks, 
               a.pcv3_weeks
        FROM tabImmunization as a           
        WHERE week(curdate()) - week (a.dob)={} 
        AND a.dob is not null
    '''.format(IMMTests['14w-11m'])
    

    _qry = db.query(_sql).export('json')

    if _qry != '[]':
        q.enqueue(
            background_tasks,
            ttl=_ttl, 
            args=('PCV3', 'PENTA3', 'OPV3'),
            kwargs={
                'data': _qry,
                'sms_msg': msg, 
                'range': '14w-11m'               
            })
    
    
    _sql = '''
        SELECT (week(curdate()) - week (a.dob)) as weeks, a.facility, 
               a.phone, a.dob, a.client_gsm_number, a.name_of_client,
               a.client_number,  
               a.vitamina_months1
        FROM tabImmunization as a           
        WHERE  week(curdate()) - week (a.dob) ={}               
        AND a.dob is not null
    '''.format(IMMTests['6m-11m'])
    
    _qry = db.query(_sql).export('json')

    if _qry != '[]':
        q.enqueue(
            background_tasks,
            ttl=_ttl, 
            args=('VITAMIN A',),
            kwargs={
                'data': _qry,
                'sms_msg': msg,
                'range': '6m-11m'                
            })



    _sql = '''
        SELECT (week(curdate()) - week (a.dob)) as weeks, 
               a.facility, a.client_gsm_number, a.name_of_client,
               a.phone, a.dob, 
               a.client_number,  
               a.measles1_months1,
               a.yellow_fever_months1
        FROM tabImmunization as a           
        WHERE week(curdate()) - week (a.dob)={}             
        AND a.dob is not null
    '''.format(IMMTests['9m-11m'])

    _qry = db.query(_sql).export('json')

    if _qry != '[]':
        q.enqueue(
            background_tasks,
            ttl=_ttl, 
            args=('MEASLES 1', 'YELLOW FEVER'),
            kwargs={
                'data': _qry,
                'sms_msg': msg, 
                'range': '9m-11m'               
            })
    

    _sql = '''
        SELECT (week(curdate()) - week (a.dob)) as weeks, 
                a.facility, a.client_gsm_number, a.name_of_client,
                a.phone, a.dob, 
                a.client_number, 
                a.opv1_months,
                a.penta1_months,
                a.pcv1_months,
                a.opv2_months,
                a.penta2_months,
                a.pcv2_months,
                a.opv3_months,
                a.penta3_months,
                a.pcv3_months,
                a.vitamina_months2,
                a.measles1_months2,
                a.yellow_fever_months2

        FROM tabImmunization as a           
        WHERE week(curdate()) - week (a.dob)={}
        AND a.dob is not null

    '''.format(IMMTests['12m-23m'])
    _qry = db.query(_sql).export('json')

    if _qry != '[]':
        q.enqueue(
            background_tasks,
            ttl=_ttl, 
            args=('OPV 1', 'PENTA1', 'PCV1', 'OPV2', 
                    'PENTA2', 'PCV2', 'OPV3', 'PENTA3', 
                    'PVC3', 'VITAMINA', 'MEASLES 1', 
                    'YELLOW FEVER'),
            kwargs={
                'data': _qry,
                'sms_msg': msg,
                'range': '12m-23m'                
            })


    
    _sql = '''
        SELECT (week(curdate()) - week (a.dob)) as weeks, a.facility, 
               a.phone, a.dob, a.client_gsm_number, a.name_of_client,
               a.client_number,  
               a.measles2_months
        FROM tabImmunization as a           
        WHERE week(curdate()) - week (a.dob)={}
        AND a.dob is not null
    '''.format(IMMTests['18m-23m'])

    _qry = db.query(_sql).export('json')

    if _qry != '[]':
        q.enqueue(
            background_tasks,
            ttl=_ttl, 
            args=('MEASLES 2',),
            kwargs={
                'data': _qry,
                'sms_msg': msg,
                'range': '18m-23m'                
            })
    


def background_tasks(*arg, **kwargs):

    import records, json
    import os, configobj
    import requests as r      

    cfg_data = configobj.ConfigObj('sites/scheduler.cfg')['app']
    cfg_file = os.path.join(cfg_data['path'], os.path.join(cfg_data['sitename'], cfg_data['config_file']))

    json_content = ''

    with open(cfg_file, 'r') as fl:
        json_content = fl.read()
        
    json_obj = json.loads(json_content)
    db_passwd = json_obj['db_password']
    db_name = json_obj['db_name']

    conn_str = 'mysql+mysqldb://%s:%s@localhost:3306/%s' %(db_name, db_passwd, db_name)

    db = records.Database(conn_str)

    _sql = '''SELECT field, value  
        FROM tabSingles 
        WHERE field in ('message_parameter', 'receiver_parameter', 'sms_gateway_url', 'use_post')
        AND doctype='SMS Settings'
    '''
    
    row = db.query(_sql)

    url = ''
    is_post = False
    _data = {}

    param = {}

    for rec in row.all():
        param[rec.field] = rec.value


    _sql = '''SELECT header, parameter, value  FROM `tabSMS Parameter` '''
    row = db.query(_sql)
    _headers = {}

    for rec in row.all():
        if rec.header == 0:
            _data[rec.parameter] = rec.value
        
        elif rec.header == 1:
            _headers[rec.parameter] = rec.value


    def send_sms(phone, msg):

        _data[param['receiver_parameter']] = phone        
        _data[param['message_parameter']] = msg
        
        url = param['sms_gateway_url']
        is_post = param['use_post']

        if is_post == '1':
            retv = r.post(url, headers=_headers, data=_data)
        elif is_post == '0':
            retv = r.get(url, headers=_headers, data=_data)

        log_event(cfg_data['logpath'], retv.text)



    def set_reminders(tretmnt, field, wk, dt_data, msg):
        # sms message parameters 
        # patient_name 
        # mobile
        # pid 
        # facility
        # treatement
        # weeks 

        tmp = {
            'mobile': dt_data['client_gsm_number'],
            'patient_name': dt_data['name_of_client'],
            'pid': dt_data['client_number'],
            'treatment': tretmnt,
            'weeks': "{} {}".format(dt_data[wk], wk)
        }

        tmp.update(dt_data)

        if dt_data[field] == 0:
            _msg = build_template(msg, **tmp)
            send_sms(tmp['mobile'], _msg)


    dt_data = json.loads(kwargs['data'])

    for item in dt_data:

        if kwargs['range'] == '0-2wk':
            
            set_reminders('OPV 0', 'opv0_weeks', 'days', item, kwargs['sms_msg'])
            set_reminders('Hep.B 0', 'hepb0_weeks', 'days', item, kwargs['sms_msg'])            

        elif kwargs['range'] == '0-11m':
            set_reminders('BCG', 'bcg_months', 'weeks', item, kwargs['sms_msg'])


        elif kwargs['range'] == '6w-11m':

            set_reminders('OPV 1', 'opv1_weeks', 'weeks', item, kwargs['sms_msg'])
            set_reminders('PENTA1', 'penta1_weeks', 'weeks', item, kwargs['sms_msg'])
            set_reminders('PCV1', 'pcv1_weeks', 'weeks', item, kwargs['sms_msg'])


        elif kwargs['range'] == '10w-11m':
            set_reminders('OPV 2', 'opv2_weeks', 'weeks', item, kwargs['sms_msg'])
            set_reminders('PENTA2', 'penta2_weeks', 'weeks', item, kwargs['sms_msg'])
            set_reminders('PCV2', 'pcv2_weeks', 'weeks', item, kwargs['sms_msg'])
            

        elif kwargs['range'] == '14w-11m':
            set_reminders('OPV 3', 'opv3_weeks', 'weeks', item, kwargs['sms_msg'])
            set_reminders('PENTA3', 'penta3_weeks', 'weeks', item, kwargs['sms_msg'])
            set_reminders('PCV3', 'pcv3_weeks', 'weeks', item, kwargs['sms_msg'])
            

        elif kwargs['range'] == '6m-11m':
            set_reminders('VITAMIN A', 'vitamina_months1', 'weeks', item, kwargs['sms_msg'])
            

        elif  kwargs['range'] == '9m-11m':
            set_reminders('MEASLES 1', 'measles1_months1', 'weeks', item, kwargs['sms_msg'])
            set_reminders('YELLOW FEVER', 'yellow_fever_months1', 'weeks', item, kwargs['sms_msg'])


        elif kwargs['range'] == '12m-23m':
            set_reminders('OPV 1', 'opv1_months', 'weeks', item, kwargs['sms_msg'])
            set_reminders('PENTA1', 'penta1_months', 'weeks', item, kwargs['sms_msg'])
            set_reminders('PCV1', 'pcv1_months', 'weeks', item, kwargs['sms_msg'])
            set_reminders('OPV2', 'opv2_months', 'weeks', item, kwargs['sms_msg'])
            set_reminders('PENTA2', 'penta2_months', 'weeks', item, kwargs['sms_msg'])
            set_reminders('PCV2', 'pcv2_months', 'weeks', item, kwargs['sms_msg'])
            set_reminders('OPV3', 'opv3_months', 'weeks', item, kwargs['sms_msg'])
            set_reminders('PENTA3', 'penta3_months', 'weeks', item, kwargs['sms_msg'])
            set_reminders('PVC3', 'pcv3_months', 'weeks', item, kwargs['sms_msg'])
            set_reminders('VITAMIN A', 'vitamina_months2', 'weeks', item, kwargs['sms_msg'])
            set_reminders('MEASLES 1', 'measles1_months2', 'weeks', item, kwargs['sms_msg'])
            set_reminders('YELLOW FEVER', 'yellow_fever_months2', 'weeks', item, kwargs['sms_msg'])
            

        elif kwargs['range'] == '18m-23m':
            set_reminders('MEASLES 2', 'measles2_months', 'weeks', item, kwargs['sms_msg'])
            

 


def build_template(str_data, **kwargs):
    from jinja2 import Template
    tmpl = Template(str_data)
    return tmpl.render(**kwargs)


def log_event(path, msg):
    import logging
    from logging.handlers import RotatingFileHandler

    logger = logging.getLogger("Rotating Log")
    logger.setLevel(logging.INFO)

    handler = RotatingFileHandler(path, maxBytes=1024,
                                  backupCount=5)
    logger.addHandler(handler)
    logger.info(msg)