// Copyright (c) 2017, omar and contributors
// For license information, please see license.txt

frappe.ui.form.on('Antenatal', {
	refresh: function (frm) {
		
	},
	onload: function (frm) {
		if(frm.doc.dob){
			$(frm.fields_dict['age_html'].wrapper).html("AGE : " + get_age(frm.doc.dob));
		}

		if (frm.doc.lmp){
 			
 			calc_pregnancy_weeks(frm);
 			
			frm.set_df_property("lmp", "read_only", frm.doc.__islocal ? 0 : 1);
			frm.refresh();


		}
	},

	lmp: function(frm){
		calc_pregnancy_weeks(frm);
	}

	

});


// this works 

frappe.ui.form.on("Antenatal", "dob", function(frm) {
	if(frm.doc.dob){
		var today = new Date();
		var birthDate = new Date(frm.doc.dob);
		var age_str = get_age(frm.doc.dob);
		$(frm.fields_dict['age_html'].wrapper).html("AGE : " + age_str);
	}
});

 

var calc_pregnancy_weeks = (frm) => {

	frappe.call({
	    method: "nhmis.nhmis.doctype.antenatal.antenatal.get_weeks_elasped",
		args: {'dte': frm.doc.lmp},		   			     

	    callback: function(resp) {		   
	    	$(frm.fields_dict['lmp_html'].wrapper).html("Pregnancy weeks elaspsed : " + resp.message);
		    }
	});
}


var get_age = function (birth) {
	var ageMS = Date.parse(Date()) - Date.parse(birth);
	var age = new Date();
	age.setTime(ageMS);
	var years = age.getFullYear() - 1970;
	return years + " Year(s) " + age.getMonth() + " Month(s) " + age.getDate() + " Day(s)";
};


