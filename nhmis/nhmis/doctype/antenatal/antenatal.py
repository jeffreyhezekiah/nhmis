# -*- coding: utf-8 -*-
# Copyright (c) 2017, omar and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from frappe.core.doctype.sms_settings.sms_settings import send_sms

import datetime 


class Antenatal(Document):
    def get_age(self):
        age_str = ""
        if self.dob:
            born = getdate(self.dob)
            age = dateutil.relativedelta.relativedelta(getdate(), born)
            age_str = str(age.years) + " year(s) " + str(age.months) + " month(s) " + str(age.days) + " day(s)"
        return age_str


@frappe.whitelist()
def get_weeks_elasped(dte):

    y,m,d = dte.split('-')
    return calculate_weeks_elasped(                                   
                                    datetime.date(int(y), int(m), int(d))
                                )



def calculate_weeks_elasped(date_recorded):
    # confirm this with Victor 

    ls = datetime.date(date_recorded.year, 
        date_recorded.month, date_recorded.day)

    _now = datetime.datetime.now()
    curr_date = datetime.date(
                _now.year, _now.month, _now.day)

    diff = curr_date - ls 
    days_elaspsed = diff.days + 1 
    in_weeks = int(days_elaspsed / 7)

    weeks_elasped =  in_weeks
    
    return weeks_elasped


def send_checkup_reminders():
    
    """ 
        # 1st Visit = 16 Weeks from LMP
        # 2nd Visit = 20 Weeks from LMP
        # 3rd Visit = 24 Weeks from LMP
        # 4th Visit = 28 Weeks from LMP
        # Expected Delivery (EDD) = 40 weeks from LMP
    """   
    msg = frappe.db.get_value("Custom SMS", None, "antenatal_sms_reminders")

    _sql = '''
        SELECT a.name, a.age_of_pregnancy, 
               a.name_of_client, 
               a.lmp, a.sms_track, 
               b.phone, b.mobile, b.email, 
               b.facility
        FROM tabAntenatal as a  
        LEFT OUTER JOIN tabPatient b 
        ON b.patient_name=a.name_of_client        
        WHERE schedule_sms=1 
    '''
    _qry = frappe.db.sql(_sql)

    for q in _qry:
        
        _phone = q[5] or q[6]
        _pat_name = q[2]
        _facility = q[8]
        _sms_track = q[4]
        _date_created = q[3]
        preg_count = q[1]

        weeks_elasped = calculate_weeks_elasped(_date_created)

        context = {'patient_name':_pat_name, 
                   'company_name':_facility or '', 
                   'pregnancy_age':weeks_elasped
                }

        msg = frappe.render_template(msg, {'doc': context, 'alert': context})

        if weeks_elasped == 16:                
            propagate_sms(_phone, msg, _pat_name, _sms_track, weeks_elasped)

        elif weeks_elasped == 20:
            propagate_sms(_phone, msg, _pat_name, _sms_track, weeks_elasped)            

        elif weeks_elasped == 24:
            propagate_sms(_phone, msg, _pat_name, _sms_track, weeks_elasped)

        elif weeks_elasped == 28:
            propagate_sms(_phone, msg, _pat_name, _sms_track, weeks_elasped)

        elif weeks_elasped == 40:
            propagate_sms(_phone, msg, _pat_name, _sms_track, weeks_elasped)
            
        elif weeks_elasped > 40:
            # terminate the scheduler here 

            _sql = """update tabAntenatal
                set schedule_sms=0
                where name_of_client='{0}'
            """.format(_pat_name)
            frappe.db.sql(_sql)
            frappe.db.commit()


def propagate_sms(phone, msg, patient, cur_sms_track, weeks):

    _track = str2list(cur_sms_track)
    temp_wk = str(weeks)

    if temp_wk in _track:
        # this will prevent sms broadcast send multiple times 
        return False

    _track.append(temp_wk)

    send_sms([phone], msg)
    
    temp = ','.join(_track)

    _sql = """UPDATE tabAntenatal SET sms_track='{0}'
        WHERE name_of_client='{1}'
    """.format(temp, patient)
    frappe.db.sql(_sql)
    frappe.db.commit()


 

def str2list(value):
    temp = value or ''
    out = temp.split(',')

    retv = []
    for x in out:
        if x:
            retv.append(str(x))

    return retv 





