# -*- coding: utf-8 -*-
# Copyright (c) 2018, omar and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class Referral(Document):
	pass
	







def add_referal_button():
	frappe.get_doc({
		"doctype":"Custom Script",
		"dt": 'Consultation',
		"script": """frappe.ui.form.on('Consultation', {
refresh: function(frm) {
	frm.add_custom_button(__('Referral'), function() {
		frappe.set_route("Form", "Referral", "New Referral");
	},"Create");
}
});"""
	}).insert(ignore_permissions=True)