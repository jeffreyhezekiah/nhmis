// Copyright (c) 2017, omar and contributors
// For license information, please see license.txt

frappe.ui.form.on('Child Delivery', {
	refresh: function (frm) {
		
	},
	onload: function (frm) {
		if(frm.doc.dob){
			$(frm.fields_dict['age_html'].wrapper).html("AGE : " + get_age(frm.doc.dob));
		}
	},

	client_number: function(frm, cdt, cdn){

		let set_default_fields = (resp) =>{
	    	frm.set_value('dob', resp['dob'])
	    	frm.set_value('name_of_client', resp['patient_name'])
	    	frm.refresh();    	
	    }
   
	  	frappe.db.get_value(
	  		'Patient', {name: frm.doc.client_number}, 
			[
				'dob', 'email', 'phone', 'card_no', 
				'patient_name',
			],
			set_default_fields
	  	);
	},

	facility: function(frm, cdt, cdn){
		var item = locals[cdt][cdn];

		let set_default_fields = (resp) =>{
	    	frm.set_value('lga', resp['local_government_area'])
	    	frm.set_value('state', resp['state'])
	    	frm.set_value('ward', resp['ward'])
	    	frm.set_value('sector', resp['facility_type'])
	    	
	    	frm.refresh();    	
	    }
 
	    frappe.db.get_value(
	  		'Facility', {facility_name: item.facility}, 
			['local_government_area', 'state', 'ward', 'facility_type'],
			set_default_fields
	  	);	   
	}


});

frappe.ui.form.on("Child Delivery", "dob", function(frm) {
	if(frm.doc.dob){
		var today = new Date();
		var birthDate = new Date(frm.doc.dob);
		var age_str = get_age(frm.doc.dob);
		$(frm.fields_dict['age_html'].wrapper).html("AGE : " + age_str);
	}
});


var get_age = function (birth) {
	var ageMS = Date.parse(Date()) - Date.parse(birth);
	var age = new Date();
	age.setTime(ageMS);
	var years = age.getFullYear() - 1970;
	return years + " Year(s) " + age.getMonth() + " Month(s) " + age.getDate() + " Day(s)";
};
