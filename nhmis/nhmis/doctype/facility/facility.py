# -*- coding: utf-8 -*-
# Copyright (c) 2017, omar and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class Facility(Document):
	def get_state(self,lga):
		state = frappe.db.sql("select state from `tabLocal Government Area` where name='{0}'".format(lga))
		if state:
			return state[0][0]

