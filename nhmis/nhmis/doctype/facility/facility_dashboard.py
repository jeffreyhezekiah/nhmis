from frappe import _

def get_data():
	return {
		'heatmap': True,
		'transactions': [
			{
				'label': _(''),
				'items': ['Antenatal', 'Out Patient', 'Patient Appointment','Consultation']
			},
			{
				'label': _(''),
				'items': ['Family Planning', 'Nutrition and Growth Planning', 'Immunization', 'Child Delivery']
			}
		]
	}