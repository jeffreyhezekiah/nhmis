# encoding: utf-8
# Copyright (c) 2013, omar and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _
from frappe.utils import formatdate, getdate, flt, add_days

def execute(filters=None):
	columns, data = get_columns(filters), get_data(filters)
	return columns, data


def get_columns(filters):
	return [
		_("Date") + "::120",
		_("Name of client") + "::120",
		_("Sex") + "::120",
		_("Client number") + ":Link/Patient:180",
		_("Date of birth") + "::120",
		_("Age in months") + "::120",
		_("Type of visit") + "::120",
		_("Breast feeding") + "::120",
		_("Stopped B/F") + "::120",
		_("Height") + "::120",
		_("Weight") + "::120",
		_("Nutritional Status") + "::120",
		_("Vitamin A supplement") + "::120",
		_("Children receive Deworming tablet") + "::120",
		_("Children referred/ admitted into CMAM program") + "::120",
		_("Eligible for SAM treatment") + "::120",
		_("Outcome of Treatment") + "::120"

		]



def get_data(filters):
	
	li_list=frappe.db.sql("select * from `tabNutrition and Growth Planning` ",as_dict=1)	

	data = []
	for item in li_list:
		row = [
		item.date,
		item.name_of_client,
		item.sex,
		item.client_number,
		item.dob,
		item.dob,
		item.type_of_visit,
		item.breast_feeding,
		item.stopped_bf,
		item.height,
		item.weight,
		item.nutritional_status,
		item.vitamin_a_supplement,
		item.children_receive_deworming_tablet,
		item.cmam_program,
		item.eligible_for_sam_treatment,
		item.outcome_of_treatment,

		]
		data.append(row)
	return data

