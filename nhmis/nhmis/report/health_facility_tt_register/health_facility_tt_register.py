# encoding: utf-8
# Copyright (c) 2013, omar and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _
from frappe.utils import formatdate, getdate, flt, add_days

def execute(filters=None):
	columns, data = get_columns(filters), get_data(filters)
	return columns, data


def get_columns(filters):
	return [
		_("Date") + "::120",
		_("Client's Card no.") + "::120",
		_("Client Age") + "::120",
		_("Client Name") + "::120",
		_("Client Follow Up Address") + "::120",
		_("Client GSM Number") + "::120",
		_("TT1") + "::120",
		_("TT2") + "::120",
		_("TT3") + "::120",
		_("TT4") + "::120",
		_("TT5") + "::120",
		_("Comments") + "::120"

		]



def get_data(filters):
	
	li_list=frappe.db.sql("select * from `tabImmunization` ",as_dict=1)	

	data = []
	for item in li_list:
		row = [
		item.date,
		item.card_no,
		item.dob,
		item.name_of_client,
		item.address,
		item.client_gsm_number,
		item.tt_1st,
		item.tt_2nd,
		item.tt_3rd,
		item.tt_4th,
		item.tt_5th,
		item.comments,		

		]
		data.append(row)
	return data

