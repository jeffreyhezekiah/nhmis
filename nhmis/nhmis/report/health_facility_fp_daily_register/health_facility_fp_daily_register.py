# encoding: utf-8
# Copyright (c) 2013, omar and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _
from frappe.utils import formatdate, getdate, flt, add_days

def execute(filters=None):
	columns, data = get_columns(filters), get_data(filters)
	return columns, data


def get_columns(filters):
	return [
		_("Date") + "::120",
		_("Name of Client") + "::120",
		_("Client Number") + ":Link/Patient:180",
		_("Address / Telephone number") + "::120",
		_("Sex") + "::120",
		_("Age") + "::120",
		_("Source") + "::120",
		_("Parity") + "::120",
		_("Counselled on FP") + "::120",
		_("Name of pill") + "::120",
		_("Administering Type") + "::120",
		_("Quantity given") + "::120",
		_("Name of Injectable") + "::120",
		_("Administering Type") + "::120",
		_("IN") + "::120",
		_("OUT") + "::120",
		_("Type of Condom") + "::120",
		_("Administering Type") + "::120",
		_("Quantity given") + "::120",
		_("Type of Implant") + "::120",
		_("IN") + "::120",
		_("OUT") + "::120",
		_("Revisit") + "::120",
		_("STERILISATION") + "::120",
		_("CYCLE BEADS") + "::120",
		_("OTHERS") + "::120",
		_("OR") + "::120",
		_("IJ") + "::120",
		_("IP") + "::120",
		_("IUCD") + "::120",
		_("SR") + "::120",
		_("MR") + "::120",
		_("FP Clients accessed HCT service") + "::120",
		_("HIV counsel, test & result") + "::120",
		_("HIV test result: positive") + "::120",
		_("HIV counsel, test & result") + "::120",
		_("HIV test result: positive") + "::120"

		]



def get_data(filters):
	
	li_list=frappe.db.sql("select * from `tabFamily Planning` ",as_dict=1)	

	data = []
	for item in li_list:
		row = [
		item.date,
		item.name_of_client,
		item.client_number,
		item.address,
		item.sex,
		item.dob,
		item.source,
		item.parity,
		item.counselled_on_fp,
		item.name_of_pill,
		item.pill_type,
		item.pill_qty,
		item.name_of_injectable,
		item.injection_type,
		item.iucd_in,
		item.iucd_out,
		item.type_of_condom,
		item.condoms_type,
		item.condoms_qty,
		item.type_of_implant,
		item.implant_in,
		item.implant_out,
		item.implant_rv,
		item.sterilisation,
		item.cycle_others,
		item.others,
		item.orp,
		item.ij,
		item.ip,
		item.iucd,
		item.sr,
		item.mr,
		item.fp_clients_accessed_hct_service,
		item.male_hiv_counsel,
		item.male_test_result,
		item.female_hiv_counsel,
		item.female_test_result,

		]
		data.append(row)
	return data

