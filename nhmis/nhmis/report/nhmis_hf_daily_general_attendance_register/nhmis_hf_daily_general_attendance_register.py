# encoding: utf-8
# Copyright (c) 2013, omar and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _
from frappe.utils import formatdate, getdate, flt, add_days

def execute(filters=None):
	columns, data = get_columns(filters), get_data(filters)
	return columns, data


def get_columns(filters):
	return [
		_("Date") + "::120",
		_("Name of patient/client") + "::120",
		_("Patient/client card number") + "::120",
		_("Date of birth") + "::120",
		_("Male") + "::120",
		_("Female") + "::120",
		_("Contact address") + "::120",
		_("State of origin") + "::120",
		_("Telephone no.") + "::120",
		_("First contact with facility") + "::120",
		_("Referred in") + "::120",
		_("Name") + "::120",
		_("Relationship with client") + "::120",
		_("Contact address") + "::120",
		_("Telephone no.") + "::120"

		]



def get_data(filters):
	
	li_list=frappe.db.sql("select * from `tabDaily general attendance register` ",as_dict=1)	

	data = []
	for item in li_list:
		row = [
		item.date,
		item.name_of_client,
		item.card_number,
		item.dob,
		item.male,
		item.female,
		item.address,
		item.state_of_origin,
		item.patient_no,
		item.first_contact,
		item.referred_in,
		item.name1,
		item.relationship_with_client,
		item.contact_address,
		item.telephone_no,

		]
		data.append(row)
	return data

