# Copyright (c) 2013, omar and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _
from frappe.utils import formatdate, getdate, flt, add_days

def execute(filters=None):
	columns, data = get_columns(filters), get_data(filters)
	return columns, data


def get_columns(filters):
	return [
		_("Date") + "::120",
		_("Name of patient") + "::120",
		_("Patient number") + ":Link/Patient:180",
		_("Sex") + "::120",
		_("Age") + "::120",
		_("Type of attendance") + "::120",
		_("Presenting complaint") + "::120",
		_("Diagnosis") + "::120",
		_("Type of laboratory investigation done") + "::120",
		_("Drugs given") + "::120",
		_("Outcome of visits") + "::120",
		_("Clinical Diagnosis Only") + "::120",
		_("RDT") + "::120",
		_("Microscopy") + "::120",
		_("Confirmed Uncomplicated Malaria") + "::120",
		_("Severe Malaria") + "::120",
		_("ACT given") + "::120",
		_("Other Antimalarial given") + "::120",
		_("HIV counsel, test & result") + "::120",
		_("HIV test result: positive") + "::120",
		_("HIV counsel, test & result") + "::120",
		_("HIV test result: positive") + "::120",
		_("Couple Counselling & Testing") + "::120",
		_("Couple Sero-discordant Test") + "::120",
		_("Male") + "::120",
		_("Female") + "::120",
		_("Male") + "::120",
		_("Female") + "::120",
		_("HCT clients provided with SRH/HIV integrated Services") + "::120",
		_("HCT client referred for FP method") + "::120",
		_("HCT clients screened for STIs") + "::120",
		_("HCT clients treated for STIs") + "::120",
		_("Clinically screened for TB") + "::120",
		_("TB clinical screening score") + "::120",
		_("HIV positive recieving ART and TB treatment") + "::120"

		]



def get_data(filters):
	
	li_list=frappe.db.sql("select * from `tabOut Patient` ",as_dict=1)	

	data = []
	for item in li_list:
		row = [
		item.date,
		item.name_of_client,
		item.client_number,
		item.sex,
		item.dob,
		item.type_of_attendance,
		item.presenting_complaint,
		item.diagnosis,
		item.type_of_laboratory_investigation_done,
		item.drugs_given,
		item.outcome_of_visits,
		item.clinical_diagnosis_only,
		item.rdt,
		item.microscopy,
		item.confirmed_uncomplicated_malaria,
		item.severe_malaria,
		item.act_given,
		item.other_antimalarial_given,
		item.male_hiv_counsel,
		item.male_hiv_test,
		item.female_hiv_counsel,
		item.female_hiv_test,
		item.couple_counselling,
		item.couple_sero,
		item.hiv_male,
		item.hiv_female,
		item.receiving_male,
		item.receiving_female,
		item.hct_srh_hiv,
		item.hct_fb,
		item.hct_screened,
		item.hct_stl,
		item.clinically_screened_for_tb,
		item.tb_clinical_screening_score,
		item.hiv_positive_recieving__art_and_tb_treatment,

		]
		data.append(row)
	return data

