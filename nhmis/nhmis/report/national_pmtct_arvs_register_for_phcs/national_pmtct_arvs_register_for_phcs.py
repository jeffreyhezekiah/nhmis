# encoding: utf-8
# Copyright (c) 2013, omar and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _
from frappe.utils import formatdate, getdate, flt, add_days

def execute(filters=None):
	columns, data = get_columns(filters), get_data(filters)
	return columns, data


def get_columns(filters):
	return [
		_("Date") + "::120",
		_("PHC Card No.") + "::120",
		_("ANC Reg. No.") + "::120",
		_("Unique ID") + "::120",
		_("Pregnant Women time of HIV diagnosis") + "::120",
		_("ART eligibility assesment") + "::120",
		_("ARV Regimen at start") + "::120",
		_("Month 0") + "::120",
		_("Month 1") + "::120",
		_("Month 2") + "::120",
		_("Month 3") + "::120",
		_("Month 4") + "::120",
		_("Month 5") + "::120",
		_("Month 6") + "::120",
		_("Month 7") + "::120",
		_("Month 8") + "::120",
		_("Month 9") + "::120",
		_("In Labour") + "::120",
		_("Breast Feeding") + "::120",
		_("HIV-exposed infant given SdNVP Within 72 hrs of life") + "::120",
		_("<2month") + "::120",
		_("2 months & above") + "::120",
		_("HIV-exposed infant on ARV prophylaxis and Breastfeeding") + "::120",
		_("DNA-PCR") + "::120",
		_("HIV Rapid test") + "::120",
		_("HIV-exposed infant tested HIV- negative to HIV Rapid test at 18 months") + "::120"

		]



def get_data(filters):
	
	li_list=frappe.db.sql("select * from `tabAntenatal` where previous_hiv = 'Yes'",as_dict=1)	

	data = []
	for item in li_list:
		row = [
		item.date,
		item.card_no,
		item.anc_reg_no,
		item.unique_id,
		item.pregnant_women_time_of_hiv_diagnosis,
		item.art_eligibility_assesment,
		item.arv_regimen_at_start,
		item.month_0,
		item.month_1,
		item.month_2,
		item.month_3,
		item.month_4,
		item.month_5,
		item.month_6,
		item.month_7,
		item.month_8,
		item.month_9,
		item.in_labour,
		item.breast_feeding,
		item.hiv_exposed,
		item.month2,
		item.months_above,
		item.hiv_exposed_infant,
		item.dna_pcr,
		item.hiv_rapid_test,
		item.hiv_rapid_test18,

		]
		data.append(row)
	return data

