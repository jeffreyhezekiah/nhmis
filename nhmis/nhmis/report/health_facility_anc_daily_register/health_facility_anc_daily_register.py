# encoding: utf-8
# Copyright (c) 2013, omar and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _
from frappe.utils import formatdate, getdate, flt, add_days

def execute(filters=None):
	columns, data = get_columns(filters), get_data(filters)
	return columns, data


def get_columns(filters):
	return [
		_("Date") + "::120",
		_("Patient Name") + "::120",
		_("Patient") + ":Link/Patient:180",
		_("Age") + "::120",
		_("Type of Visit") + "::120",
		_("Parity") + "::120",
		_("Antenatal Clinic Attendance") + "::120",
		_("Age of pregnancy") + "::120",
		_("No. of antenatal clinic visit(s) to date") + "::120",
		_("ANC HIV counseling & testing") + "::120",
		_("Partners of HIV pos. pregnant women") + "::120",
		_("Partners of HIV neg. pregnant women") + "::120",
		_("Testing") + "::120",
		_("Treated") + "::120",
		_("HB/PCV") + "::120",
		_("Sugar") + "::120",
		_("Proteins") + "::120",
		_("LLIN given") + "::120",
		_("Doses of IPT given") + "::120",
		_("Hematinics Given") + "::120",
		_("TT indicate") + "::120",
		_("Associated problems") + "::120",
		_("Outcome of visit") + "::120",
		_("Mother & Newborn") + "::120",
		_("Neonatal Complications") + "::120",
		_("KMC") + "::120"

		]



def get_data(filters):
	
	li_list=frappe.db.sql("select * from `tabAntenatal` where previous_hiv = 'No'",as_dict=1)	

	data = []
	for item in li_list:
		row = [
		item.date,
		item.name_of_client,
		item.client_number,
		item.dob,
		item.type_of_visit,
		item.parity,
		item.antenatal_clinic_attendance,
		item.age_of_pregnancy,
		item.antenatal_visits_number,
		item.anc_counseling,
		item.partners_pos_pregnant_women,
		item.partners_neg_pregnant_women,
		item.testing,
		item.treated,
		item.hb_pcv,
		item.sugar,
		item.proteins,
		item.llin_given,
		item.doses_of_ipt_given,
		item.hematinics_given,
		item.tt_indicate,
		item.associated_problems,
		item.outcome_of_visit,
		item.mother_newborn,
		item.neonatal_complications,
		item.kmc,

		]
		data.append(row)
	return data

