# encoding: utf-8
# Copyright (c) 2013, omar and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _
from frappe.utils import formatdate, getdate, flt, add_days

def execute(filters=None):
	columns, data = get_columns(filters), get_data(filters)
	return columns, data


def get_columns(filters):
	return [
		_("Date") + "::120",
		_("Patient Name") + "::120",
		_("Patient no.") + ":Link/Patient:180",
		_("Age") + "::120",
		_("Type of Client") + "::120",
		_("Parity") + "::120",
		_("HIV Counseling Testing at L&D") + "::120",
		_("Date Of Delivery") + "::120",
		_("Mode Of Delivery") + "::120",
		_("Pertograph Used") + "::120",
		_("Actre Managment of 3rd Stage of Labour used") + "::120",
		_("Mearned Couple Holeth Sean") + "::120",
		_("Alive") + "::120",
		_("Dead") + "::120",
		_("Wow media counducted") + "::120",
		_("Aborthon") + "::120",
		_("prereturn") + "::120",
		_("Either auphyske") + "::120",
		_("Live birth") + "::120",
		_("Still birth") + "::120",
		_("Dead") + "::120",
		_("Sex Of Baby") + "::120",
		_("Who took delivery of child") + "::120",
		_("Immerdere Now born care bror blod") + "::120",
		_("Excheuse Breast Deeing Alam") + "::120",
		_("Name of Person Who took delivery") + "::120"

		]



def get_data(filters):
	
	li_list=frappe.db.sql("select * from `tabChild Delivery` ",as_dict=1)	

	data = []
	for item in li_list:
		row = [
		item.date,
		item.name_of_client,
		item.client_number,
		item.dob,
		item.type_of_client,
		item.parity,
		item.hiv_counseling_testing,
		item.date_of_delivery,
		item.mode_of_delivery,
		item.pertograph_used,
		item.actre_managment_of_3rd_stage_of_labour_used,
		item.mearned_couple_holeth_sean,
		item.alive,
		item.dead,
		item.wow_media_counducted,
		item.aborthon,
		item.prereturn,
		item.either_auphyske,
		item.live_birth,
		item.still_birth,
		item.baby_dead,
		item.sex_of_baby,
		item.who_took_delivery_of_child,
		item.immerdere_now_born_care_bror_blod,
		item.excheuse_breast_deeing_alam,
		item.name_of_person_who_took_delivery,

		]
		data.append(row)
	return data

