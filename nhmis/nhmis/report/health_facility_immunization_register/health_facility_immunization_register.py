# encoding: utf-8
# Copyright (c) 2013, omar and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _
from frappe.utils import formatdate, getdate, flt, add_days

def execute(filters=None):
	columns, data = get_columns(filters), get_data(filters)
	return columns, data


def get_columns(filters):
	return [
		_("Date") + "::120",
		_("Session Type") + "::120",
		_("Outreach Site Name") + "::120",
		_("Date") + "::120",
		_("Date") + "::120",
		_("Date") + "::120",
		_("Date") + "::120",
		_("Date") + "::120",
		_("Date") + "::120",
		_("Date") + "::120",
		_("Date") + "::120",
		_("Date") + "::120",
		_("Date") + "::120",
		_("Date") + "::120",
		_("Date") + "::120",
		_("6 - 11 MONTHS Date") + "::120",
		_("12 - 23 MONTHS Date") + "::120",
		_("Date") + "::120",
		_("Date") + "::120",
		_("Date") + "::120",
		_("Date") + "::120",
		_("CONJUGATE A CSM") + "::120",
		_("1ST") + "::120",
		_("2ND") + "::120",
		_("3RD") + "::120",
		_("4TH") + "::120",
		_("5TH") + "::120",
		_("Comments") + "::120"

		]



def get_data(filters):
	
	li_list=frappe.db.sql("select * from `tabImmunization` ",as_dict=1)	

	data = []
	for item in li_list:
		row = [
		item.date,
		item.session_type,
		item.outreach_site_name,
		item.opv0_date,
		item.hepb_date,
		item.bcg_date,
		item.opv1_date,
		item.penta1_date,
		item.pcv1_date,
		item.opv2_date,
		item.penta2_date,
		item.pcv2_date,
		item.opv3_date,
		item.penta3_date,
		item.pcv3_date,
		item.vitamina_date1,
		item.vitamina_date2,
		item.measles1_date,
		item.yellow_fever_date,
		item.measles2_date,
		item.conjugate_csm_date,
		item.tt_1st,
		item.tt_2nd,
		item.tt_3rd,
		item.tt_4th,
		item.tt_5th,
		item.comments,		

		]
		data.append(row)
	return data

